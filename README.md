## Credit and caveats/disclaimers

The skin and design thereof, as well as the original code to make it look that way are the work of the [TMA Unofficial Transcripts site](https://snarp.github.io/magnus_archives_transcripts/), who you should absolutely credit.

Turning it into an AO3 workskin was the work of [shinyopals](https://archiveofourown.org/users/shinyopals/) and while I would appreciate a nod (or a link to the [original fic](https://archiveofourown.org/works/26584894)), I think it's more important to give credit to the transcripts site!


**General disclaimer**: I am super not a programmer, I did this for fun and the code may not be perfect. Feel free to make it better. If you're having trouble using this skin then feel free to chuck me an ask [on tumblr](https://shinyopals.tumblr.com/) and I'll help if I can, but fair warning: I work long-ass hours in a full time job, so it might be a while before I reply.

**Note on colours**: 
The font colouring of all three types of text (name rows, dialogue rows, and sound effects) are a shade darker than the original transcipt site. This is because - at least on my PC - I found AO3's additional styling (possibly the impact of the additional white background) made the sound effects in particular too difficult to read. If you want the original colour values then you'll need to find and replace within workskin.css:

1. #5a6e75 to #657b83
2. #4f6369 to #586e75
3. #849090 to #93a1a1


---

## How to use

AO3 has a guide to creating a custom workskin [here](https://archiveofourown.org/faq/skins-and-archive-interface?language_id=en#createworkskin) and you should use that for how to create and apply a custom skin. You will need to write your fic in **html code** to be able to use this skin.

1. Use the file workskin.css and copy/paste the code from that into your custom skin
2. Take the file FicCode.html and paste into your preferred text editor - this shows how all of your fic will need to be written - any line breaks/formatting/etc will not be applied unless you code them, and you won't be able to easily use AO3's richtext editor (I didn't even try so I don't know if it's functional at all wit this level of coding)
3. The DIVs called wrapper/midwrap at the top, and closing </div>s at the end only need to be there once - at the very top and the very end
4. The scene in FicCode.html shows you which type of code is needed for which type of text - as well as using hr for scene breaks - just copy and paste as you go

---

## Note about testing on AO3

1. Fun fact, AO3's 'Save as draft' can cause an error resulting in your fic not showing in the tags if you do that and then later post that draft - so if you do use 'Save as draft', be sure to discard and start from scratch if you do then post
2. ('Preview' without save seems to work fine??)

---

## How to create a file to share with a beta/friend - without AO3 

If you want to test pre-AO3, or create a document you can share, you can do that - you need a text editor or some software that'll let you save text as html. If you're using Windows, Notepad is fine. You can also download something like Notepad++ or SublimeText for free if you fancy it.

1. Paste the workskin.css code into notepad
2. At the very top of it, type **<style>**
3. At the very bottom of it, type **</style>**
4. Find and replace **#workskin** with blank to get rid of it
5. Paste in your fic below **</style>**
6. Save your text file with the extension .html
7. You should now be able to open this file in your browser of choice - or send to a friend

NOTE: the formatting should be **largely** correct, but it won't match AO3 perfectly - AO3 applies its own code style code too. In particular, the descriptive/sound effect paragraph spacing was really squashed in my browser. It was fine on AO3, but for beta purposes I used find/replace to add/remove additional html line breaks.